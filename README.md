# Termux_Package

Termux_Package

Es un instalador de paquetes básicos para Termux.
Ideal para los novatos que vienen empezando con el uso
De Termux.


Pasos de Instalación en Termux:

Actualizamos Repositorios...

apt update && apt upgrade -y

Instalamos los Requisitos del Script...

apt install git figlet -y


Clonamos el Repositorio:

git clone https://github.com/Fabr1x/Termux_Package.git 

Seleccionamos el Fichero..


cd Termux_Package

Listamos


ls

Damos permisos de ejecución:

chmod 777 *;ls


Ejecutamos Termux-Package.sh


bash Termux-Package.sh


Acepte lo que le diga el script y listo!!



Created: F@br1x and 你好😜

